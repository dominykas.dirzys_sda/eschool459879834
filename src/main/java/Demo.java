import com.google.gson.internal.bind.util.ISO8601Utils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person petras = new Person("Petras", "Bumblauskas", LocalDate.of(1986, 12, 5), 70f, 1.80f, Gender.MALE);
        Person kazys = new Person("Kazys", "Jonaitis", LocalDate.of(1988, 6, 8), 80f, 1.85f, Gender.MALE);
        Person egle = new Person("Eglė", "Petrauskaitė", LocalDate.of(2003, 1, 23), 50f, 1.68f, Gender.FEMALE);
        Person[] persons = {petras, kazys, egle};

        Stream<Person> personsStream = Arrays.stream(persons);
        personsStream.filter(pers -> pers.getAge() >= Person.ADULT_AGE)
                .forEach(pers -> System.out.println(pers));
        System.out.println("Something smells :(");
    }
}
